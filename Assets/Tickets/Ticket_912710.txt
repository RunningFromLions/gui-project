[Store Number]: 
744744

[Customer First Name]: 
Pizza Hut

[Customer Last Name]: 
744744

[Contact Name]: 
Store

[Incident Main Summary]: 
ttyp51 Touch Screen not detected. 

[Incident Main Notes]: 


[Service Type]: 
User Service Restoration

[Work Detail Section - Summary]: 
ttyp51 Touch Screen not detected. 

[Work Detail Section - Notes]: 


Name: ttyp51
IP Address: 10.1.2.51
Uptime: 3 days, 14:51,
Run_Level: 4
Motherboard: DH61WW
Motherboard Manufacturer: INTEL CORPORATION
Printer Configured?: Yes
Printer Device Node: /dev/rcprinter
Printer Attached: None
Printer SimLink: 
Cash Drawer Configured: No
Touch Scrren: No Touch Screen Detected
Drive Health: No Issues Detected
Printer Errors: No Issues Detected
Packet Loss: 0

Step 1:
Locate Terminal ttyp51 and it's Touch Screen. Turn the power to both devices off.

Time On Step 3.983272

Step 2:
On the back or bottom of the touch screen, there is a USB port that should connect the touch screen to the computer. Unplug this cable from the monitor, wait for 30 seconds and then plug it back in.
If this cable is missing, please attempt to locate it and plug it back in.

Time On Step 0.7620666

Step 3:
On the back of the terminal, the other end of the USB cable should be plugged in to a USB port. Please disconnect the cable for 30 seconds and then reconnect it. If the cable is NOT connected, plug it in to an available USB port.
You may now power the monitor and terminal back on.

Time On Step 0.3644671



[Assigned Company]: 
Pizza Hut

[Assigned Organization]: 
PHI

[Assigned Group]: 
PHI Help Desk L1

[Incident Status]: 
Resolved

[Incident Number]: 


[Operational Categorization Tier 1]: 
Failed

[Operational Categorization Tier 2]: 
N/A

[Operational Categorization Tier 3]: 
N/A

[Product Categorization Tier 1]: 
Above Store

[Product Categorization Tier 2]: 
Automation

[Product Categorization Tier 3]: 
Monitoring

[Product Name]: 
Alerts

[Resolution Product Categorization Tier 1]: 
Above Store

[Resolution Product Categorization Tier 2]: 
Automation

[Resolution Product Categorization Tier 3]: 
Monitoring

[Resolution Product Name]: 
Alerts

[Resolution]: 
Resolved by utility

